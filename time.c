#include <stdio.h>

#include <stdlib.h>

#include <ctype.h>

#include <string.h>

struct t_time {
  int hora;
  int min;
  int seg;
};
typedef struct t_time time;

struct t_timetable {
  time key;
  char val[50];
};
typedef struct t_timetable timetable;

time stringToTime(char hour[10]) {
  time timeCreated;
  char * token = strtok(hour, ":");
  timeCreated.hora = atoi(token);
  token = strtok(NULL, ":");
  timeCreated.min = atoi(token);
  token = strtok(NULL, ":");
  timeCreated.seg = atoi(token);
  return timeCreated;
}

int timeCmp(time h1, time h2) {
  // 1  se h1 >  h2
  // 0  se h1 == h2
  // -1 se h1 <  h2

  if (h1.hora == h2.hora && h1.min == h2.min && h1.seg == h2.seg) {
    return 0;
  } else if (h1.hora > h2.hora) {
    return 1;
  } else if (h1.hora == h2.hora) {
    if (h1.min > h2.min) {
      return 1;
    } else if (h1.min < h2.seg) {
      return -1;
    } else if (h1.seg > h2.seg) {
      return 1;
    } else if (h1.seg < h2.seg) {
      return -1;
    }
  } else return -1;
}

void put(timetable table[100], time key, char val[50]) {

  int i = 0;
  while (strcmp(table[i].val, "0") != 0) {
    i++;
  }
  table[i].key = key;
  strcpy(table[i].val, val);
}

char * get(timetable table[100], time key) {

  int i = 0;
  while (timeCmp(key, table[i].key) != 0) i++;

  return table[i].val;
}

void delete(timetable table[100], time key) {

  int i = 0, b;
  while (timeCmp(key, table[i].key) != 0) i++;
  for (i; strcmp(table[i].val, "0") !=0, ++i;) {

    table[i].key = table[i + 1].key;
    strcpy(table[i].val, table[i + 1].val);
  }
}

int contains(timetable table[100], time key) {

  int i = 0;
  while (timeCmp(key, table[i].key) != 0) i++;
  if (timeCmp(key, table[i].key) == 0) {
    return 1;
  } else {
    return 0;
  };
}

int is_empty(timetable table[100]) {

  if (strcmp(table[0].val, "0") == 0) {
    return 1;
  } else {
    return 0;
  }
}

int size(timetable table[100]) {

  int i = 0;
  while (strcmp(table[i].val, "0") != 0) i++;

  return i;
}

time max(timetable table[100]) {

  int i = 0;
  time a = table[0].key;
  while (strcmp(table[i].val, "0") != 0) {

    if (timeCmp(a, table[i].key) == -1)
      a = table[i].key;
    i++;
  }

  return a;
}

time min(timetable table[100]) {

  int i = 0;
  time a = table[0].key;
  while (strcmp(table[i].val, "0") != 0) {

    if (timeCmp(a, table[i].key) == 1)
      a = table[i].key;
    i++;
  }

  return a;
}

time floor(timetable table[100], time key) {

  int i = 0;
  time a = table[0].key;
  while (strcmp(table[i].val, "0") != 0) {

    if (timeCmp(a, table[i].key) == -1 || timeCmp(a, table[i].key) == 0)
      a = table[i].key;
    i++;
  }

  return a;
}

time ceiling(timetable table[100], time key) {

  int i = 0;
  time a = table[0].key;
  while (strcmp(table[i].val, "0") != 0) {

    if (timeCmp(a, table[i].key) == 1 || timeCmp(a, table[i].key) == 0)
      a = table[i].key;
    i++;
  }

  return a;
}

int rank(timetable table[100], time key) {

  int i = 0, count = 0;
  while (strcmp(table[i].val, "0") != 0) {

    if (timeCmp(key, table[i].key) == -1)
      count++;
    i++;
  }

  return count;
}

time select(timetable table[100], int k) {

  return table[k].key;
}

void delete_min(timetable table[100]) {

  int i = 0;
  time a = table[0].key;printf("a \n");
  while (strcmp(table[i].val, "0") != 0) {
   printf("b 1\n");
    if (timeCmp(a, table[i].key) == -1){
	
      a = table[i].key;
	  printf("c");}
       i++;
  }
  delete(table, a);
}

void delete_max(timetable table[100]) {

  int i = 0;
  time a = table[0].key;
  while (strcmp(table[i].val, "0") != 0) {
  
      if (timeCmp(a, table[i].key) == 1)
      a = table[i].key;
 	  i++;
  }

  delete(table, a);
}

int size_range(timetable table[100], int lo, int hi) {
  int i = lo, cont = 0;
  while (i < hi) {
    i++;
    cont++;
  }
  return cont;
}

int main() {
  char hour[10], city[50];
  timetable table[100];
  time processed_time, aa;
  int routes, i, aux;
  for (aux = 0; aux < 100; aux++) strcpy(table[aux].val, "0");

  printf("Quantas rotar ir� inserir?: ");
  scanf("%d", & routes);

  for (i; i < routes; i++) {

    printf("Digite a hora (HH:MM:SS): ");
    printf("\n 0 para sair\n ");

    scanf("%s", & hour);
    processed_time = stringToTime(hour);

    printf("\nDigite a cidade:\n");
    scanf("%s", & city);
    put(table, processed_time, city);

  }

  printf("Get %s \n", get(table, processed_time));
  printf("Contains %d \n", contains(table, processed_time));
  printf("isEmpty %d\n ", is_empty(table));
  printf("size %d \n", contains(table, processed_time));
  aa = min(table);
  printf("min %d%d%d\n", aa.hora, aa.min, aa.seg);
  aa = max(table);
  printf("max  %d%d%d\n", aa.hora, aa.min, aa.seg);
  aa = floor(table, processed_time);
  printf("floor %d%d%d\n", aa.hora, aa.min, aa.seg);
  aa = ceiling(table, processed_time);
  printf("ceiling%d%d%d\n", aa.hora, aa.min, aa.seg);
  printf("rank %d\n", rank(table, processed_time));
  aa = select(table, 1);
  printf("select %d%d%d\n", aa.hora, aa.min, aa.seg);
  printf("deletemin\n");
  delete_min(table);
  printf("deletemax\n");
  delete_max(table);

}
